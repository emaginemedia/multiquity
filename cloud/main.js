var util = require('util');
var request = require('request');
 
// this will be our call to our friends in Amerikay to see who their clients are to see who gets in 
exports.authenticateUser = function(params, callback) {

	console.log(JSON.stringify(params));
	thisLogin = params;

	request({
		uri: 'https://webservice.ecrvault.com/C2SAdminService/C2SAuthorization/CheckAccess',
		json: {
		    "PhoneNumber": thisLogin.phone,
		    "ZipCode": thisLogin.zip,
		    "DeviceID": thisLogin.uuid,
		    "DeviceName": thisLogin.deviceName,
		    "ApplicationVersion": thisLogin.appVersion,
		    "ApplicationName": thisLogin.appName,
		    "OSVersion": thisLogin.osVersion
		}, 
		method: "POST"
		}, function (error, response, body) {
		  console.log("Status", response.statusCode);
		  console.log("Headers", JSON.stringify(response.headers));
		  // console.log("Response received", body);
		  return callback(null, body);
	});

 };

// this is the secondary call, either once logged in previously, or when doubled up and a re-select happens for doubles
exports.authenticateUserByID = function(params, callback) {

	console.log(JSON.stringify(params));
	thisLogin = params;

	request({
		uri: 'https://webservice.ecrvault.com/C2SAdminService/C2SAuthorization/CheckAccessByID',
		json: {
		    "ClinicID": thisLogin.clinicID,
		    "DeviceID": thisLogin.uuid,
		    "DeviceName": thisLogin.deviceName,
		    "ApplicationVersion": thisLogin.appVersion,
		    "ApplicationName": thisLogin.appName,
		    "OSVersion": thisLogin.osVersion
		}, 
		method: "POST"
		}, function (error, response, body) {
		  //console.log("Status", response.statusCode);
		  //console.log("Headers", JSON.stringify(response.headers));
		  //console.log("Response received", body);
		  return callback(null, body);
	});

 };

 // this is the call to the reporting install, so we can track licenses - hard-coded for this installation, will be dynamic as we roll out
 exports.authenticateUsername = function(params, callback) {

	request({
		url: "http://www.eyequity-app.com/index.php/api/authenticate_username", 
		headers:{'content-type':'application/x-www-form-urlencoded'},
		body: 'username=ecrvault&password=comsquar3d',
		method: "POST"
	}, function (err, response, body) {
		//console.log("Status", response.statusCode);
		//console.log("Headers", JSON.stringify(response.headers));
		// console.log("Response received", body);
		//console.log(response);
		var user = JSON.parse(body);
		return callback(null, body);
	});

};

// log this entry to EE by creating a new record
 exports.authenticateLogRecord = function(params, callback) {

 	thisLogin = params;

 	var channel_id = thisLogin.channel_id;
    var title =  thisLogin.clinicid;
    var session_id  = thisLogin.session_id;
    var entry_date = thisLogin.entry_date;
    var clinicid = thisLogin.clinicid;
    var clinicname = thisLogin.clinicname;
    var deviceid = thisLogin.deviceid;
    var devicename = thisLogin.devicename;
    var applicationversion =  thisLogin.applicationversion;
    var applicationname = thisLogin.applicationname;
    var osversion = thisLogin.osversion;

	request({
		url: "http://www.eyequity-app.com/index.php/api/create_channel_entry", 
		headers:{'content-type':'application/x-www-form-urlencoded'},
		body: 'channel_id='+channel_id+'&title='+title+'&session_id='+session_id+'&entry_date='+entry_date+'&clinicid='+clinicid+'&clinicname='+clinicname+'&deviceid='+deviceid+'&devicename='+devicename+'&applicationversion='+applicationversion+'&applicationname='+applicationname+'&osversion='+osversion,
		method: "POST"
	}, function (err, response, body) {
		//console.log("Status", response.statusCode);
		//console.log("Headers", JSON.stringify(response.headers));
		// console.log("Response received", body);
		//console.log(response);
		//var user = JSON.parse(body);
		return callback(null, body);
	});

};
